package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.Model;
import com.epam.model.goods.Goods;
import com.epam.model.goods.enums.Category;

import java.util.Set;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }


    @Override
    public Set<Goods> getGoodsByCategory(Category category) {
        return model.getGoodsByCategory(category);
    }


    @Override
    public Category[] getAllCategories() {
        return model.getAllCategories();
    }

    @Override
    public Object[] getAllTypesOfCategory(Category category) {
        return model.getAllTypesOfCategory(category);
    }

    @Override
    public Set<Goods> getGoodsByCategoryAndType(Category category, String type) {
        return null;
    }

    @Override
    public Set<Goods> getGoodsByCategoryAndTypeAndPrice(Category category, String type, int price) {
        return model.getGoodsByCategoryAndTypeAndPrice(category, type, price);
    }
}
