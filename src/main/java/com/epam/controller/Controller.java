package com.epam.controller;

import com.epam.model.goods.Goods;
import com.epam.model.goods.enums.Category;

import java.util.Set;

public interface Controller {
    Set<Goods> getGoodsByCategory(Category category);

    Category[] getAllCategories();

    Object[] getAllTypesOfCategory(Category category);

    Set<Goods> getGoodsByCategoryAndType(Category category, String type);

    Set<Goods> getGoodsByCategoryAndTypeAndPrice(Category category, String type, int price);
}
