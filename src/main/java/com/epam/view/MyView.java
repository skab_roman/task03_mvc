package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import com.epam.model.goods.Goods;
import com.epam.model.goods.enums.Category;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class MyView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - get all goods by Category");
        menu.put("2", " 2 - get goods by prise (first method)");
        menu.put("3", " 3 - get goods by prise (second method)");
        menu.put("Q", " Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
    }

    private void pressButton1() {
        System.out.println("\nCategories:");
        for (Category category : Category.values()) {
            System.out.println(category.ordinal() + " - " + category);
        }
        System.out.println("Please, select category:");
        int menuCategory = input.nextInt();
        Set<Goods> goods = controller.getGoodsByCategory(Category.values()[menuCategory]);
        goods.forEach(System.out::println);
    }

    private void pressButton2() {
        System.out.println("\nCategories:");
        for (Category category : Category.values()) {
            System.out.println(category.ordinal() + " - " + category);
        }
        System.out.println("Please, select category:");
        int menuCategory = input.nextInt();

        System.out.println("\nTypes of " + Category.values()[menuCategory].toString().toLowerCase() + "s:");
        Set<Goods> goodsSet = controller.getGoodsByCategory(Category.values()[menuCategory]);
        Class<?> typeClass = goodsSet.stream().findFirst().get().getType().getClass();
        Method valuesMethod = null;
        try {
            valuesMethod = typeClass.getMethod("values");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        valuesMethod.setAccessible(true);
        Object[] objects = null;
        try {
            objects = (Object[]) valuesMethod.invoke(typeClass);
            for (int i = 0; i < objects.length; i++) {
                System.out.println(i + " - " + objects[i]);
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.out.println("Please, select " + typeClass.getSimpleName() + ":");
        int menuType = input.nextInt();
        String selectedType = objects[menuType].toString();
        Set<Goods> filteredByType = goodsSet.stream().filter(goods -> goods.getType().toString().equals(selectedType)).collect(Collectors.toSet());

        System.out.println("\nPlease, enter max price of " + selectedType.toLowerCase() + ":");
        int maxPrice = input.nextInt();
        Set<Goods> filteredByPrice = filteredByType.stream().filter(goods -> goods.getPrice() <= maxPrice).collect(Collectors.toSet());
        filteredByPrice.forEach(System.out::println);
    }

    private void pressButton3() {
        Category selectedCategory;
        String selectedType;
        int maxPrice;

        System.out.println("\nCategories:");
        Category[] categories = controller.getAllCategories();
        for (Category category : categories) {
            System.out.println(category.ordinal() + " - " + category);
        }
        System.out.println("Please, select category:");
        int menuCategory = input.nextInt();
        selectedCategory = Category.values()[menuCategory];

        System.out.println("\nType of " + selectedCategory.toString().toLowerCase() + ":");
        Object[] types = controller.getAllTypesOfCategory(selectedCategory);
        for (int i = 0; i < types.length; i++) {
            System.out.println(i + " - " + types[i]);
        }
        System.out.println("Please, select type of " + selectedCategory.toString().toLowerCase() + ":");
        int menuType = new Scanner(System.in).nextInt();
        selectedType = types[menuType].toString();

        System.out.println("\nPlease, enter max price of " + selectedType.toLowerCase() + ":");
        maxPrice = input.nextInt();

        Set<Goods> goodsSet = controller.getGoodsByCategoryAndTypeAndPrice(selectedCategory, selectedType, maxPrice);
        goodsSet.forEach(System.out::println);

    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point:");
            keyMenu = new Scanner(System.in).nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
