package com.epam.model;

import com.epam.model.goods.Goods;
import com.epam.model.goods.enums.Category;

import java.util.Set;

public class BusinessLogic implements Model {
    private Domain domain;

    public BusinessLogic() {
        domain = new Domain();
    }

    @Override
    public Set<Goods> getGoodsByCategory(Category category) {
        return domain.getGoodsByCategory(category);
    }


    public Category[] getAllCategories() {
        return domain.getAllCategories();
    }

    public Object[] getAllTypesOfCategory(Category category) {
        return domain.getAllTypesOfCategory(category);
    }

    public Set<Goods> getGoodsByCategoryAndType(Category category, String type) {
        return domain.getGoodsByCategoryAndType(category, type);
    }

    public Set<Goods> getGoodsByCategoryAndTypeAndPrice(Category category, String type, int price) {
        return domain.getGoodsByCategoryAndTypeAndPrice(category, type, price);
    }
}
