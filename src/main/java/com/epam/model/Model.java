package com.epam.model;

import com.epam.model.goods.Goods;
import com.epam.model.goods.enums.Category;

import java.util.Set;

public interface Model {
    Set<Goods> getGoodsByCategory(Category category);

    public Category[] getAllCategories();

    public Object[] getAllTypesOfCategory(Category category);

    public Set<Goods> getGoodsByCategoryAndType(Category category, String type);

    public Set<Goods> getGoodsByCategoryAndTypeAndPrice(Category category, String type, int price);

}
