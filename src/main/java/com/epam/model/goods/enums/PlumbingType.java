package com.epam.model.goods.enums;

public enum PlumbingType {
    WASHBASIN,
    TOILET_BOWL,
    BATHTAB
}
