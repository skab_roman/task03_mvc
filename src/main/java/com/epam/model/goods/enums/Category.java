package com.epam.model.goods.enums;

public enum Category {
    PAINT,
    PLUMBING,
    WOODEN_PRODUCT
}
