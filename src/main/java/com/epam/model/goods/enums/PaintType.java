package com.epam.model.goods.enums;

public enum PaintType {
    VARNISH,
    ENAMEL,
    PRIMER
}
