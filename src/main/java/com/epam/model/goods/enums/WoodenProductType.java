package com.epam.model.goods.enums;

public enum WoodenProductType {
    DOOR,
    FLOOR,
    WINDOW
}
