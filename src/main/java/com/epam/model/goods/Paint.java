package com.epam.model.goods;

import com.epam.model.goods.enums.Category;
import com.epam.model.goods.enums.PaintType;

public class Paint extends Goods {
    private PaintType type;

    public Paint(String brand, String unit, Double price, Category category, PaintType type) {
        super(brand, unit, price, category);
        this.type = type;
    }

    @Override
    public PaintType getType() {
        return type;
    }

    public void setType(PaintType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Paint)) return false;
        if (!super.equals(o)) return false;

        Paint paint = (Paint) o;

        return getType() == paint.getType();

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getType().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Paint{" +
                super.toString() +
                "type=" + type +
                '}';
    }
}
