package com.epam.model.goods;

import com.epam.model.goods.enums.Category;
import com.epam.model.goods.enums.WoodenProductType;

public class WoodenProduct extends Goods{
    private WoodenProductType type;

    public WoodenProduct(String brand, String unit, Double price, Category category, WoodenProductType type) {
        super(brand, unit, price, category);
        this.type = type;
    }

    @Override
    public WoodenProductType getType() {
        return type;
    }

    public void setType(WoodenProductType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WoodenProduct)) return false;
        if (!super.equals(o)) return false;

        WoodenProduct that = (WoodenProduct) o;

        return getType() == that.getType();

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getType().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "WoodenProduct{" +
                super.toString() +
                "type=" + type +
                '}';
    }
}
