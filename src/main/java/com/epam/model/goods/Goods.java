package com.epam.model.goods;

import com.epam.model.goods.enums.Category;

public abstract class Goods {
    private String brand;
    private String unit;
    private Double price;
    private Category category;

    public Goods() {
    }

    public abstract <T> T getType();

    public Goods(String brand, String unit, Double price, Category category) {
        this.brand = brand;
        this.unit = unit;
        this.price = price;
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Goods)) return false;

        Goods goods = (Goods) o;

        if (!getBrand().equals(goods.getBrand())) return false;
        if (!getUnit().equals(goods.getUnit())) return false;
        if (!getPrice().equals(goods.getPrice())) return false;
        return getCategory() == goods.getCategory();

    }

    @Override
    public int hashCode() {
        int result = getBrand().hashCode();
        result = 31 * result + getUnit().hashCode();
        result = 31 * result + getPrice().hashCode();
        result = 31 * result + getCategory().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "brand='" + brand + '\'' +
                ", unit='" + unit + '\'' +
                ", price=" + price +
                ", category=" + category +
                '}';
    }
}
