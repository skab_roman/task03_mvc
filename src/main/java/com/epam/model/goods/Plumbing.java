package com.epam.model.goods;

import com.epam.model.goods.enums.Category;
import com.epam.model.goods.enums.PlumbingType;

public class Plumbing extends Goods {
    private PlumbingType type;

    public Plumbing(String brand, String unit, Double price, Category category, PlumbingType type) {
        super(brand, unit, price, category);
        this.type = type;
    }

    @Override
    public PlumbingType getType() {
        return type;
    }

    public void setType(PlumbingType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Plumbing)) return false;
        if (!super.equals(o)) return false;

        Plumbing plumbing = (Plumbing) o;

        return getType() == plumbing.getType();

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getType().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Plumbing{" +
                super.toString() +
                "type=" + type +
                '}';
    }
}
