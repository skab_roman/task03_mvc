package com.epam.model;

import com.epam.model.goods.Goods;
import com.epam.model.goods.Paint;
import com.epam.model.goods.Plumbing;
import com.epam.model.goods.WoodenProduct;
import com.epam.model.goods.enums.Category;
import com.epam.model.goods.enums.PaintType;
import com.epam.model.goods.enums.PlumbingType;
import com.epam.model.goods.enums.WoodenProductType;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Domain {
    private Set<Goods> goodsList;

    public Domain() {
        goodsList = new LinkedHashSet();
        goodsList.add(new Plumbing("Kolo", "piece", 4999.99, Category.PLUMBING, PlumbingType.TOILET_BOWL));
        goodsList.add(new Plumbing("Gybner", "piece", 3888.88, Category.PLUMBING, PlumbingType.TOILET_BOWL));
        goodsList.add(new Plumbing("Kolo", "piece", 1888.88, Category.PLUMBING, PlumbingType.WASHBASIN));
        goodsList.add(new Plumbing("Gybner", "piece", 1999.99, Category.PLUMBING, PlumbingType.WASHBASIN));
        goodsList.add(new Plumbing("Kolo", "piece", 10000.00, Category.PLUMBING, PlumbingType.BATHTAB));
        goodsList.add(new Plumbing("Gybner", "piece", 8000.00, Category.PLUMBING, PlumbingType.BATHTAB));

        goodsList.add(new WoodenProduct("Grand", "piece", 4000.00, Category.WOODEN_PRODUCT, WoodenProductType.DOOR));
        goodsList.add(new WoodenProduct("Albero", "piece", 5000.00, Category.WOODEN_PRODUCT, WoodenProductType.DOOR));
        goodsList.add(new WoodenProduct("Grand", "m2", 800.00, Category.WOODEN_PRODUCT, WoodenProductType.FLOOR));
        goodsList.add(new WoodenProduct("Albero", "m2", 900.00, Category.WOODEN_PRODUCT, WoodenProductType.FLOOR));
        goodsList.add(new WoodenProduct("Grand", "m2", 1600.00, Category.WOODEN_PRODUCT, WoodenProductType.WINDOW));
        goodsList.add(new WoodenProduct("Albero", "m2", 1400.00, Category.WOODEN_PRODUCT, WoodenProductType.WINDOW));

        goodsList.add(new Paint("Ceresit", "liter", 100.00, Category.PAINT, PaintType.VARNISH));
        goodsList.add(new Paint("Sniezka", "liter", 110.00, Category.PAINT, PaintType.VARNISH));
        goodsList.add(new Paint("Ceresit", "liter", 90.00, Category.PAINT, PaintType.ENAMEL));
        goodsList.add(new Paint("Sniezka", "liter", 80.00, Category.PAINT, PaintType.ENAMEL));
        goodsList.add(new Paint("Ceresit", "liter", 40.00, Category.PAINT, PaintType.PRIMER));
        goodsList.add(new Paint("Sniezka", "liter", 50.00, Category.PAINT, PaintType.PRIMER));
    }

    public Set<Goods> getGoodsByCategory(Category category) {
        switch (category) {
            case PAINT:
                return goodsList.stream().filter(goods -> goods
                        .getCategory()
                        .equals(Category.PAINT))
                        .collect(Collectors.toSet());
            case PLUMBING:
                return goodsList.stream().filter(goods -> goods
                        .getCategory()
                        .equals(Category.PLUMBING))
                        .collect(Collectors.toSet());
            case WOODEN_PRODUCT:
                return goodsList.stream().filter(goods -> goods
                        .getCategory()
                        .equals(Category.WOODEN_PRODUCT))
                        .collect(Collectors.toSet());
            default:
                return null;
        }
    }


    public Category[] getAllCategories() {
        return Category.values();
    }

    public Object[] getAllTypesOfCategory(Category category) {
        Object[] types = null;
        Object type = goodsList.stream().filter(goods -> goods.getCategory().equals(category)).findFirst().get().getType();
        Class<?> typeClass = type.getClass();
        Method valuesMethod = null;
        try {
            valuesMethod = typeClass.getMethod("values");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        valuesMethod.setAccessible(true);
        try {
            types = (Object[]) valuesMethod.invoke(typeClass);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return types;
    }

    public Set<Goods> getGoodsByCategoryAndType(Category category, String type) {
        return null;
    }

    public Set<Goods> getGoodsByCategoryAndTypeAndPrice(Category category, String type, int price) {
        Set<Goods> goodsSet = goodsList.stream().filter(goods -> goods.getCategory().equals(category) && goods.getType().toString().equals(type) && goods.getPrice()<=price).collect(Collectors.toSet());
        return goodsSet;
    }
}
